/*常用工具*/

/**
 * 拷贝二维数组
 * @param obj
 * @returns {Array}
 */
function copy(obj) {
    var out = [],i = 0,len = obj.length;
    for (; i < len; i++) {
        if (obj[i] instanceof Array){
            out[i] = copy(obj[i]);
        }
        else out[i] = obj[i];
    }
    return out;
}

/**
 * 读取json文件
 * @param filename 文件名
 * @param callback 回调函数
 */
function getJson(filename,callback){
    var test;
    if(window.XMLHttpRequest){
        test = new XMLHttpRequest();
    }else if(window.ActiveXObject){
        test = new window.ActiveXObject();
    }else{
        console.log("浏览器不兼容");
    }
    if(test !=null){
        test.open("GET",filename,true);
        test.send(null);
        test.onreadystatechange=function(){
            if(test.readyState==4&&test.status==200){
                var obj=JSON.parse(test.responseText);
                callback(obj);
            }
        };
    }
}