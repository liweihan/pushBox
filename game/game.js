/*游戏主要逻辑*/

var x=0;//当前位置 横坐标
var y=0;//当前位置 纵坐标
var step=0;//总步数
var isWin=false;//是否胜利

/**
 * 监听键盘事件
 * @param event
 */
document.onkeydown=function(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && (e.keyCode==87||e.keyCode==38)){ // 按 W 或 up
        up();
    }
    if(e && (e.keyCode==65||e.keyCode==37)){ // 按 A 或 left
        left();
    }
    if(e && (e.keyCode==83||e.keyCode==40)){ // 按 S 或 down
        down();
    }
    if(e && (e.keyCode==68||e.keyCode==39)){ // 按 D 或 right
        right();
    }
};

/**
 * 向上
 */
function up() {
    //获取玩家当前位置
    getPlayerLocation();
    //移动
    let curr={x:x,y:y};
    let target={x:x-1,y:y};
    let after={x:x-2,y:y};
    move(curr,target,after);
}

/**
 * 向下
 */
function down() {
    //获取玩家当前位置
    getPlayerLocation();
    //移动
    let curr={x:x,y:y};
    let target={x:x+1,y:y};
    let after={x:x+2,y:y};
    move(curr,target,after);
}

/**
 * 向左
 */
function left() {
    //获取玩家当前位置
    getPlayerLocation();
    //移动
    let curr={x:x,y:y};
    let target={x:x,y:y-1};
    let after={x:x,y:y-2};
    move(curr,target,after);
}

/**
 * 向右
 */
function right() {
    //获取玩家当前位置
    getPlayerLocation();
    //移动
    let curr={x:x,y:y};
    let target={x:x,y:y+1};
    let after={x:x,y:y+2};
    move(curr,target,after);
}

/**
 * 移动
 * @param curr 当前位置
 * @param target 目标位置
 * @param after 目标位置后
 */
function move(curr,target,after) {
    //如果目标位置是空地或目的地
    if(map[target.x][target.y]==empty||map[target.x][target.y]==end){
        map[target.x][target.y]+=player;
        map[curr.x][curr.y]-=player;
        addStep();
    }
    //如果目标位置是箱子 并且箱子后边是空地
    if(map[target.x][target.y]==box&&map[after.x][after.y]==empty){
        map[after.x][after.y]=box;
        map[target.x][target.y]=player;
        map[curr.x][curr.y]-=player;
        addStep();
    }
    //如果目标位置是箱子 并且箱子后边是目的地
    if(map[target.x][target.y]==box&&map[after.x][after.y]==end){
        map[after.x][after.y]=done;
        map[target.x][target.y]=player;
        map[curr.x][curr.y]-=player;
        addStep();
    }
    //如果目标位置是箱子+目的地 并且箱子后边还是目的地
    if(map[target.x][target.y]==done&&map[after.x][after.y]==end){
        map[after.x][after.y]=done;
        map[target.x][target.y]=pe;
        map[curr.x][curr.y]-=player;
        addStep();
    }
    //如果目标位置是箱子+目的地 并且箱子后边是空地
    if(map[target.x][target.y]==done&&map[after.x][after.y]==empty){
        map[after.x][after.y]=box;
        map[target.x][target.y]=pe;
        map[curr.x][curr.y]-=player;
        addStep();
    }
    //重新加载地图
    getMap();
}

/**
 * 增加步数
 */
function addStep() {
    if(!isWin){
        document.getElementById("status").innerHTML="步数："+(++step);
        win();
    }
}

/**
 * 胜利
 */
function win() {
    for (var i=0;i<h;i++){
        for (var j=0;j<w;j++){
            if(map[i][j]==end||map[i][j]==pe) return;
        }
    }
    document.getElementById("win").style.display="block";
    isWin=true;
}

/**
 * 重新开始
 */
function reset() {
    setMap(level);
    isWin=false;
    step=0;
    document.getElementById("win").style.display="none";
    document.getElementById("level").innerHTML="第 "+(level+1)+" 关";
    document.getElementById("status").innerHTML="步数："+step;
    getMap();
}

/**
 * 下一关
 */
function next() {
    if(maps.length==level+1){
        alert("已是最后一关");
        return;
    }
    level++;
    reset();
}

/**
 * 上一关
 */
function pre() {
    if(level==0){
        alert("已是第一关");
        return;
    }
    level--;
    reset();
}